package org.heiner.budget.config;

public class ConfigurationException extends RuntimeException {
	private static final long serialVersionUID = 5275267346694777079L;

	public ConfigurationException(final String message) {
		super(message);
	}
}
