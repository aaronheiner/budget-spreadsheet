package org.heiner.budget.config.account;

import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum NamedAccount {
	CITIBANK_CARD, DISCOVER_CARD, LMCU_MAX_CHECKING, LMCU_MEMBER_SAVINGS, ROCKET_MORTGAGE;

	public static Stream<NamedAccount> stream() {
		return Stream.of(values());
	}
}
