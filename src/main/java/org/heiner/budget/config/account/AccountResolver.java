package org.heiner.budget.config.account;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.heiner.budget.config.ConfigurationException;
import org.heiner.budget.domain.account.Account;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AccountResolver {
	private Map<NamedAccount, Account> namedAccounts = new HashMap<>();

	public Account getNamedAccount(final NamedAccount namedAccount) {
		log.info("Looking up named account for {}", namedAccount);
		return Optional.ofNullable(namedAccounts.get(namedAccount)).orElseThrow(() -> new ConfigurationException(
				String.format("Unable to resolve named account for %s", namedAccount)));
	}

	void addNamedAccount(final NamedAccount namedAccount, final Account account) {
		log.info("Adding account {} as {}", account, namedAccount);
		namedAccounts.put(namedAccount, account);
	}
}
