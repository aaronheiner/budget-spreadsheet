package org.heiner.budget.config.account;

import java.math.BigDecimal;
import java.util.Optional;

import org.heiner.budget.domain.account.Account;
import org.heiner.budget.domain.account.CheckingAccount;
import org.heiner.budget.domain.account.CreditCard;
import org.heiner.budget.domain.account.Loan;
import org.heiner.budget.domain.account.SavingsAccount;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccountConfiguration {
	@Bean
	public AccountResolver accountResolver() {
		final AccountResolver accountResolver = new AccountResolver();

		accountResolver.addNamedAccount(NamedAccount.CITIBANK_CARD, citibankCreditCard());
		accountResolver.addNamedAccount(NamedAccount.DISCOVER_CARD, discoverCreditCard());
		accountResolver.addNamedAccount(NamedAccount.LMCU_MAX_CHECKING, lmcuMaxChecking());
		accountResolver.addNamedAccount(NamedAccount.LMCU_MEMBER_SAVINGS, lmcuMemberSavings());
		accountResolver.addNamedAccount(NamedAccount.ROCKET_MORTGAGE, rocketMortgage());

		return accountResolver;
	}

	private Account citibankCreditCard() {
		final BigDecimal openingBalance = BigDecimal.valueOf(13.99);
		final BigDecimal interestRate = BigDecimal.valueOf(0.1999);
		return new CreditCard("Citibank Credit Card", openingBalance, interestRate);
	}

	private Account discoverCreditCard() {
		final BigDecimal openingBalance = BigDecimal.valueOf(345.67);
		final BigDecimal interestRate = BigDecimal.valueOf(0.0999);
		return new CreditCard("Discover Credit Card", openingBalance, interestRate);
	}

	private Account lmcuMaxChecking() {
		final BigDecimal openingBalance = BigDecimal.valueOf(5123.45);
		final BigDecimal interestRate = BigDecimal.valueOf(0.030);
		return new CheckingAccount("LMCU Max Checking", openingBalance, Optional.of(interestRate));
	}

	private Account lmcuMemberSavings() {
		final BigDecimal openingBalance = BigDecimal.valueOf(5.00);
		final BigDecimal interestRate = BigDecimal.valueOf(0.001);
		return new SavingsAccount("LMCU Member Savings", openingBalance, interestRate);
	}

	private Account rocketMortgage() {
		final BigDecimal openingBalance = BigDecimal.valueOf(100000.00);
		final BigDecimal interestRate = BigDecimal.valueOf(0.02875);
		return new Loan("Rocket Mortgage", openingBalance, interestRate);
	}
}
