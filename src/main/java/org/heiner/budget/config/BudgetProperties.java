package org.heiner.budget.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "annual-budget")
/** Properties used to control annual budget generation */
public class BudgetProperties {
	/** Flag used to determine if a file should be auto-saved when program runs */
	private boolean autoSave = true;
	/** Working directory where the annual budget file should be saved */
	private String workingDirectory;
	/** Year to generate an annual budget for */
	private int year = 2020;
}
