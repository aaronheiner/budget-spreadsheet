package org.heiner.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BudgetSpreadsheetApplication {
	public static void main(String[] args) {
		SpringApplication.run(BudgetSpreadsheetApplication.class, args);
	}
}
