package org.heiner.budget.util;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StreamUtility {
	private static final int INITIAL_COUNTER_VALUE = 1;

	public static <T> Consumer<T> withSelfIncrementingCounter(BiConsumer<Integer, T> consumer) {
		final AtomicInteger counter = new AtomicInteger(INITIAL_COUNTER_VALUE);
		return item -> consumer.accept(counter.getAndIncrement(), item);
	}
}
