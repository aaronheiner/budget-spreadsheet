package org.heiner.budget.domain;

import java.util.List;

import org.heiner.budget.category.BudgetCategory;

public interface BudgetRecord {
	BudgetCategory getBudgetCategory();

	List<DailyLedger> getDailyLedgers();
}
