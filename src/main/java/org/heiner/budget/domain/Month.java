package org.heiner.budget.domain;

import java.time.DayOfWeek;
import java.time.YearMonth;
import java.util.Set;

import org.apache.commons.compress.utils.Sets;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum Month {
	JANUARY(1, "January"), //
	FEBRUARY(2, "February"), //
	MARCH(3, "March"), //
	APRIL(4, "April"), //
	MAY(5, "May"), //
	JUNE(6, "June"), //
	JULY(7, "July"), //
	AUGUST(8, "August"), //
	SEPTEMBER(9, "September"), //
	OCTOBER(10, "October"), //
	NOVEMBER(11, "November"), //
	DECEMBER(12, "December");

	private final int month;
	private final String description;

	public int daysInMonthForYear(final int year) {
		return YearMonth.of(year, month).lengthOfMonth();
	}

	public boolean isWeekendDay(final int year, final int dayOfMonth) {
		final Set<DayOfWeek> weekendDays = Sets.newHashSet(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

		return weekendDays.contains(YearMonth.of(year, month).atDay(dayOfMonth).getDayOfWeek());
	}
}
