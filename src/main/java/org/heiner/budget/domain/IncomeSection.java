package org.heiner.budget.domain;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.heiner.budget.category.IncomeCategory;

import lombok.Getter;

public class IncomeSection implements BudgetSection {
	@Getter
	private final String title = "Income";
	@Getter
	private final int daysInMonth;

	private Map<IncomeCategory, BudgetRecord> incomeCategories;

	private IncomeSection(final Month month, final int budgetYear) {
		this.daysInMonth = month.daysInMonthForYear(budgetYear);
		setupIncomeCategoryRecordsUsing(month, budgetYear);
	}

	public static IncomeSection forMonthAndBudgetYear(final Month month, final int budgetYear) {
		return new IncomeSection(month, budgetYear);
	}

	private void setupIncomeCategoryRecordsUsing(final Month month, final int budgetYear) {
		incomeCategories = new TreeMap<>();
		for (IncomeCategory incomeCategory : IncomeCategory.values()) {
			incomeCategories.put(incomeCategory, CategoryRecord.forBudgetCategory(incomeCategory, month, budgetYear));
		}
	}

	@Override
	public List<BudgetRecord> getBudgetRecords() {
		return List.copyOf(incomeCategories.values());
	}
}
