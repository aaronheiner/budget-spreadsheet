package org.heiner.budget.domain;

import lombok.Getter;

public class MonthlyBudget {
	@Getter
	private final Month month;
	private final int budgetYear;

	@Getter
	private final IncomeSection incomeSection;
	@Getter
	private final ExpenseSection expenseSection;

	private MonthlyBudget(final Month month, final int budgetYear) {
		this.month = month;
		this.budgetYear = budgetYear;
		this.incomeSection = IncomeSection.forMonthAndBudgetYear(month, budgetYear);
		this.expenseSection = ExpenseSection.forMonthAndBudgetYear(month, budgetYear);
	}

	public static MonthlyBudget forMonthAndBudgetYear(final Month month, final int budgetYear) {
		return new MonthlyBudget(month, budgetYear);
	}

	public String getMonthName() {
		return month.getDescription();
	}

	public String getTitle() {
		return String.format("%s %s", getMonthName(), budgetYear);
	}

	public int getDaysInMonth() {
		return month.daysInMonthForYear(budgetYear);
	}
}
