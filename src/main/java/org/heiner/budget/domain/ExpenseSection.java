package org.heiner.budget.domain;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.heiner.budget.category.ExpenseCategory;

import lombok.Getter;

public class ExpenseSection implements BudgetSection {
	@Getter
	private final String title = "Expense";
	@Getter
	private final int daysInMonth;

	private Map<ExpenseCategory, BudgetRecord> expenseCategories;

	private ExpenseSection(final Month month, final int budgetYear) {
		this.daysInMonth = month.daysInMonthForYear(budgetYear);
		setupExpenseCategoryRecordsUsing(month, budgetYear);
	}

	public static ExpenseSection forMonthAndBudgetYear(final Month month, final int budgetYear) {
		return new ExpenseSection(month, budgetYear);
	}

	private void setupExpenseCategoryRecordsUsing(final Month month, final int budgetYear) {
		expenseCategories = new TreeMap<>();
		for (ExpenseCategory expenseCategory : ExpenseCategory.values()) {
			expenseCategories.put(expenseCategory,
					CategoryRecord.forBudgetCategory(expenseCategory, month, budgetYear));
		}
	}

	@Override
	public List<BudgetRecord> getBudgetRecords() {
		return List.copyOf(expenseCategories.values());
	}
}
