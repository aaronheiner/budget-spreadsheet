package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@ToString(callSuper = true)
public class CheckingAccount extends AbstractBaseAccount {
	public CheckingAccount(final String name, final BigDecimal beginningBalance,
			final Optional<BigDecimal> interestRate) {
		super(AccountType.CHECKING, name, beginningBalance, interestRate);
	}
}
