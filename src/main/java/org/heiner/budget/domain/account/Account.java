package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

public interface Account {
	String getName();

	AccountType getAccountType();

	BigDecimal getBeginningBalance();

	Optional<BigDecimal> getInterestRate();
}
