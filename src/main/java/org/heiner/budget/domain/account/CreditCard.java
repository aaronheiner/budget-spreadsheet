package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

import lombok.ToString;

@ToString(callSuper = true)
public class CreditCard extends AbstractBaseAccount {
	public CreditCard(final String name, final BigDecimal beginningBalance, final BigDecimal interestRate) {
		super(AccountType.CREDIT_CARD, name, beginningBalance, Optional.of(interestRate));
	}
}
