package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

import lombok.ToString;

@ToString(callSuper = true)
public class Loan extends AbstractBaseAccount {
	public Loan(final String name, final BigDecimal beginningBalance, final BigDecimal interestRate) {
		super(AccountType.LOAN, name, beginningBalance, Optional.of(interestRate));
	}
}
