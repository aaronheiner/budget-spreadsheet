package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
abstract class AbstractBaseAccount implements Account {
	@EqualsAndHashCode.Include
	private final AccountType accountType;
	@EqualsAndHashCode.Include
	private final String name;
	private final BigDecimal beginningBalance;
	private final Optional<BigDecimal> interestRate;
}
