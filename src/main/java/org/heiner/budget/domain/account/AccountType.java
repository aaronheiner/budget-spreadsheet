package org.heiner.budget.domain.account;

public enum AccountType {
	CHECKING, SAVINGS, CREDIT_CARD, LOAN
}
