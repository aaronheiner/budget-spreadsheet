package org.heiner.budget.domain.account;

import java.math.BigDecimal;
import java.util.Optional;

import lombok.ToString;

@ToString(callSuper = true)
public class SavingsAccount extends AbstractBaseAccount {
	public SavingsAccount(final String name, final BigDecimal beginningBalance, final BigDecimal interestRate) {
		super(AccountType.SAVINGS, name, beginningBalance, Optional.of(interestRate));
	}
}
