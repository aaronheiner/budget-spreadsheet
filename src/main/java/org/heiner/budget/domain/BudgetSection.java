package org.heiner.budget.domain;

import java.util.List;

public interface BudgetSection {
	String getTitle();

	List<BudgetRecord> getBudgetRecords();

	int getDaysInMonth();
}
