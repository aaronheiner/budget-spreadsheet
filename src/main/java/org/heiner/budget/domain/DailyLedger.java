package org.heiner.budget.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class DailyLedger {
	private final int dayOfMonth;
	private final boolean weekend;
}
