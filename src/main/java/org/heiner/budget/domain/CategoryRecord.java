package org.heiner.budget.domain;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.heiner.budget.category.BudgetCategory;
import org.heiner.budget.category.IncomeCategory;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class CategoryRecord implements BudgetRecord {

	public static CategoryRecord forBudgetCategory(final BudgetCategory budgetCategory, final Month month,
			final int year) {
		if (IncomeCategory.PAYCHECK.equals(budgetCategory)) {
			return new PayrollBudgetRecord(budgetCategory, month, year);
		} else {
			return new BasicBudgetRecord(budgetCategory, month, year);
		}
	}

	private static class BasicBudgetRecord extends CategoryRecord {
		@Getter
		private final BudgetCategory budgetCategory;
		private Map<Integer, DailyLedger> dailyLedgers;

		private BasicBudgetRecord(final BudgetCategory budgetCategory, final Month month, final int year) {
			this.budgetCategory = budgetCategory;
			initializeBudgetDays(month, year);
		}

		private void initializeBudgetDays(final Month month, final int year) {
			this.dailyLedgers = new TreeMap<>();
			for (int dayOfMonth = 1; dayOfMonth <= month.daysInMonthForYear(year); ++dayOfMonth) {
				dailyLedgers.put(dayOfMonth, new DailyLedger(dayOfMonth, month.isWeekendDay(year, dayOfMonth)));
			}
		}

		@Override
		public List<DailyLedger> getDailyLedgers() {
			return List.copyOf(dailyLedgers.values());
		}
	}

	private static class PayrollBudgetRecord extends BasicBudgetRecord {
		private PayrollBudgetRecord(final BudgetCategory budgetCategory, final Month month, final int year) {
			super(budgetCategory, month, year);
		}
	}
}
