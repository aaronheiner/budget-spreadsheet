package org.heiner.budget.domain;

import java.util.ArrayList;
import java.util.List;

import org.heiner.budget.config.BudgetProperties;
import org.heiner.budget.config.account.AccountResolver;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class AnnualBudget {
	private final BudgetProperties budgetProperties;
	private final AccountResolver accountResolver;

	@Getter
	private List<MonthlyBudget> monthlyBudgets = new ArrayList<>();
	
	public AnnualBudget(final BudgetProperties budgetProperties, final AccountResolver accountResolver) {
		this.budgetProperties = budgetProperties;
		this.accountResolver = accountResolver;
		initializeMonthlyBudgets();
	}

	private void initializeMonthlyBudgets() {
		for (Month month : Month.values()) {
			monthlyBudgets.add(MonthlyBudget.forMonthAndBudgetYear(month, budgetProperties.getYear()));
		}
	}

	public int getBudgetYear() {
		return budgetProperties.getYear();
	}
}
