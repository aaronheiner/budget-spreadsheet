package org.heiner.budget.category;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum ExpenseCategory implements BudgetCategory {
	AUTO_GAS("Auto:Gas"), //
	AUTO_LOAN("Auto:Loan Payment"), //
	AUTO_MAINTENANCE("Auto:Maintenance"), //
	AUTO_REGISTRATION("Auto:Registration"), //
	CASH_WITHDRAWL("Cash Withdrawl"), //
	CLOTHING("Clothing"), //
	ENTERTAINMENT_BOOKS("Entertainment:Books"), //
	ENTERTAINMENT_DATE("Entertainment:Date"), //
	ENTERTAINMENT_MOVIES("Entertainment:Movies"), //
	ENTERTAINMENT_MUSIC("Entertainment:Music"), //
	FOOD_GROCERIES("Food:Groceries"), //
	FOOD_RESTAURANT("Food:Restaurant"), //
	FOOD_SCHOOL_LUNCH("Food:School Lunch"), //
	HEALTH_DENTIST("Health:Dentist"), //
	HEALTH_DOCTOR("Health:Doctor"), //
	HEALTH_EYE_CARE("Health:Eye Care"), //
	HEALTH_PHARMACY("Health:Pharmacy"), //
	HEALTH_PSYCHIATRIST("Health:Psychiatrist"), //
	HOBBY("Hobby"), //
	HOME_OWNER_ASSOCIATION("Home:HOA"), //
	HOME_IMPROVEMENT("Home:Improvement"), //
	HOME_LAWN_GARDEN("Home:Lawn & Garden"), //
	HOME_MORTGAGE("Home:Mortgage"), //
	INSURANCE("Insurance"), //
	KIDS("Kids"), //
	MISCELLANEOUS("Miscellaneous"), //
	PERSONAL_CARE_HAIR("Personal Care:Hair"), //
	PERSONAL_CARE_MISC("Personal Care:Misc"), //
	PETS_GROOMING("Pets:Grooming"), //
	PETS_SUPPLIES("Pets:Supplies"), //
	PEST_VET("Pets:Vet"), //
	UTILITIES_CELLPHONE("Utilities:Cell Phone"), //
	UTILITIES_ELECTRIC("Utilities:Electric"), //
	UTILITIES_GAS("Utilities:Gas"), //
	UTILITIES_HOMEPHONE("Utilities:Home Phone"), //
	UTILITIES_INTERNET("Utilities:Internet"), //
	UTILITIES_TELEVISION("Utilities:Television");
	
	private final String description;
}
