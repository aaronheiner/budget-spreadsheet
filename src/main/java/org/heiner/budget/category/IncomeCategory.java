package org.heiner.budget.category;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum IncomeCategory implements BudgetCategory {
	BONUS("Bonus"), //
	CASH("Cash"), //
	CHECK_DEPOSIT("Check Deposit"), //
	INTEREST("Interest"), //
	PAYCHECK("Paycheck"), //s
	TAX_RETURN("Tax Return");
	
	private final String description;
}
