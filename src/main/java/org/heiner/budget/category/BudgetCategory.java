package org.heiner.budget.category;

public interface BudgetCategory {
	String getDescription();
}
