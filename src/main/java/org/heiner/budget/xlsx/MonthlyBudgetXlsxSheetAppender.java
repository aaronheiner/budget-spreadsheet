package org.heiner.budget.xlsx;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.domain.MonthlyBudget;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class MonthlyBudgetXlsxSheetAppender {
	private final MonthlyBudgetXlsxTitleAppender sheetTitleAppender;
	private final MonthlyBudgetXlsxCategoryHeaderAppender categoryHeaderAppender;
	private final MonthlyBudgetXlsxBudgetSectionAppender budgetSectionAppender;

	public void appendSheetForMonthlyBudgetToWorkbookUsingStyles(final MonthlyBudget monthlyBudget,
			final SXSSFWorkbook workbook, final AnnualBudgetStyles workbookStyles) {

		final SXSSFSheet sheet = workbook.createSheet(monthlyBudget.getMonthName());

		sheetTitleAppender.appendMonthlyBudgetTitleToSheetUsingStyles(monthlyBudget, sheet, workbookStyles);

		final int headerRowIndex = categoryHeaderAppender
				.appendCategoryHeaderToSheetUsingStylesAndReturnRowIndex(monthlyBudget, sheet, workbookStyles);

		final int incomeSectionFinishRowIndex = budgetSectionAppender.addBudgetSectionToSheetAndReturnFinishRowIndex(
				monthlyBudget.getIncomeSection(), sheet, headerRowIndex + 1, workbookStyles);

		final int expenseSectionFinishRowIndex = budgetSectionAppender.addBudgetSectionToSheetAndReturnFinishRowIndex(
				monthlyBudget.getExpenseSection(), sheet, incomeSectionFinishRowIndex + 2, workbookStyles);
	}
}
