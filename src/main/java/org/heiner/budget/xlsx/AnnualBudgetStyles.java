package org.heiner.budget.xlsx;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import lombok.Getter;

@Getter
class AnnualBudgetStyles {
	private static final short CURRENCY_FORMAT = 8;

	private CellStyle sheetTitleStyle;
	private CellStyle categoryHeaderStyle;
	private CellStyle sectionHeaderStyle;
	private CellStyle categoryStyle;
	private CellStyle categoryTotalStyle;
	private CellStyle weekdayStyle;
	private CellStyle weekendStyle;
	private CellStyle sectionSummaryStyle;

	private AnnualBudgetStyles(final SXSSFWorkbook workbook) {
		initializeSheetTitleStyleFor(workbook);
		initializeCategoryHeaderStyleFor(workbook);
		initializeSectionHeaderStyleFor(workbook);
		initializeCategoryStylesFor(workbook);
		initializeDailyLedgerStylesFor(workbook);
		initializeSectionSummaryStyleFor(workbook);
	}

	public static AnnualBudgetStyles forWorkbook(final SXSSFWorkbook workbook) {
		return new AnnualBudgetStyles(workbook);
	}

	private void initializeSheetTitleStyleFor(final SXSSFWorkbook workbook) {
		sheetTitleStyle = workbook.createCellStyle();

		final Font font = workbook.createFont();
		font.setFontName("Arial Black");
		font.setFontHeightInPoints((short) 20);
		font.setBold(true);
		sheetTitleStyle.setFont(font);
	}

	private void initializeCategoryHeaderStyleFor(final SXSSFWorkbook workbook) {
		categoryHeaderStyle = workbook.createCellStyle();
		categoryHeaderStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		categoryHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		final Font font = workbook.createFont();
		font.setFontName("Calibri");
		font.setFontHeightInPoints((short) 11);
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		categoryHeaderStyle.setFont(font);
	}

	private void initializeSectionHeaderStyleFor(final SXSSFWorkbook workbook) {
		sectionHeaderStyle = workbook.createCellStyle();
		sectionHeaderStyle.setFillForegroundColor(IndexedColors.GREY_80_PERCENT.getIndex());
		sectionHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		final Font sectionHeaderFont = workbook.createFont();
		sectionHeaderFont.setFontName("Calibri");
		sectionHeaderFont.setFontHeightInPoints((short) 11);
		sectionHeaderFont.setColor(IndexedColors.WHITE.getIndex());
		sectionHeaderFont.setBold(true);
		sectionHeaderStyle.setFont(sectionHeaderFont);
	}

	private void initializeCategoryStylesFor(final SXSSFWorkbook workbook) {
		final Font categoryFont = workbook.createFont();
		categoryFont.setFontName("Calibri");
		categoryFont.setFontHeightInPoints((short) 11);
		categoryFont.setItalic(true);

		categoryStyle = workbook.createCellStyle();
		categoryStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		categoryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		categoryStyle.setFont(categoryFont);

		categoryTotalStyle = workbook.createCellStyle();
		categoryTotalStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		categoryTotalStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		categoryTotalStyle.setDataFormat(CURRENCY_FORMAT);
		categoryTotalStyle.setFont(categoryFont);
	}

	private void initializeDailyLedgerStylesFor(final SXSSFWorkbook workbook) {
		final Font budgetDayFont = workbook.createFont();
		budgetDayFont.setFontName("Calibri");
		budgetDayFont.setFontHeightInPoints((short) 11);

		weekdayStyle = workbook.createCellStyle();
		weekdayStyle.setFont(budgetDayFont);
		weekdayStyle.setDataFormat(CURRENCY_FORMAT);

		weekendStyle = workbook.createCellStyle();
		weekendStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
		weekendStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		weekendStyle.setFont(budgetDayFont);
		weekendStyle.setDataFormat(CURRENCY_FORMAT);
	}

	private void initializeSectionSummaryStyleFor(final SXSSFWorkbook workbook) {
		sectionSummaryStyle = workbook.createCellStyle();
		sectionSummaryStyle.setFillForegroundColor(IndexedColors.TEAL.getIndex());
		sectionSummaryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		final Font sectionSummaryFont = workbook.createFont();
		sectionSummaryFont.setFontName("Calibri");
		sectionSummaryFont.setFontHeightInPoints((short) 11);
		sectionSummaryFont.setColor(IndexedColors.WHITE.getIndex());
		sectionSummaryFont.setBold(true);
		sectionSummaryFont.setItalic(true);
		sectionSummaryStyle.setFont(sectionSummaryFont);
		sectionSummaryStyle.setDataFormat(CURRENCY_FORMAT);
	}
}
