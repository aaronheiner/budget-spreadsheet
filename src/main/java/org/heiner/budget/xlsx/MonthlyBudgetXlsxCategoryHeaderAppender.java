package org.heiner.budget.xlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.heiner.budget.domain.MonthlyBudget;
import org.springframework.stereotype.Component;

@Component
class MonthlyBudgetXlsxCategoryHeaderAppender {
	private static final int CATEGORY_HEADER_ROW_INDEX = 2;
	private static final int BLANK_COLUMN_INDEX = 0;
	private static final int CATEGORY_COLUMN_INDEX = 1;
	private static final int CATEGORY_TOTAL_COLUMN_INDEX = 2;
	private static final int PIXEL_CONVERSION_FACTOR = 37;

	public int appendCategoryHeaderToSheetUsingStylesAndReturnRowIndex(final MonthlyBudget monthlyBudget, final SXSSFSheet sheet,
			final AnnualBudgetStyles workbookStyles) {

		applyColumnWidthConstraintsToSheet(sheet, monthlyBudget.getDaysInMonth());

		final Row headerRow = sheet.createRow(CATEGORY_HEADER_ROW_INDEX);

		final Cell categoryCell = headerRow.createCell(CATEGORY_COLUMN_INDEX);
		categoryCell.setCellValue("Category");
		categoryCell.setCellStyle(workbookStyles.getCategoryHeaderStyle());

		final Cell categoryTotalCell = headerRow.createCell(CATEGORY_TOTAL_COLUMN_INDEX);
		categoryTotalCell.setCellValue("Category Total");
		categoryTotalCell.setCellStyle(workbookStyles.getCategoryHeaderStyle());

		for (int dayOfMonth = 1; dayOfMonth <= monthlyBudget.getDaysInMonth(); ++dayOfMonth) {
			final int CURRENT_CELL_INDEX = CATEGORY_TOTAL_COLUMN_INDEX + dayOfMonth;
			final Cell dayOfMonthCell = headerRow.createCell(CURRENT_CELL_INDEX);
			dayOfMonthCell.setCellValue(dayOfMonth);
			dayOfMonthCell.setCellStyle(workbookStyles.getCategoryHeaderStyle());
		}

		return CATEGORY_HEADER_ROW_INDEX;
	}

	private void applyColumnWidthConstraintsToSheet(final SXSSFSheet sheet, final int daysInMonth) {
		sheet.setColumnWidth(BLANK_COLUMN_INDEX, 64 * PIXEL_CONVERSION_FACTOR);
		sheet.setColumnWidth(CATEGORY_COLUMN_INDEX, 220 * PIXEL_CONVERSION_FACTOR);
		sheet.setColumnWidth(CATEGORY_TOTAL_COLUMN_INDEX, 97 * PIXEL_CONVERSION_FACTOR);

		for (int dayOfMonth = 1; dayOfMonth <= daysInMonth; ++dayOfMonth) {
			final int CURRENT_CELL_INDEX = CATEGORY_TOTAL_COLUMN_INDEX + dayOfMonth;
			sheet.setColumnWidth(CURRENT_CELL_INDEX, 75 * PIXEL_CONVERSION_FACTOR);
		}
	}
}
