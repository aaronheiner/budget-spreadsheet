package org.heiner.budget.xlsx.util;

import java.io.File;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component(value = "xlsxWorkbookWriter")
@ConditionalOnProperty(prefix = "annual-budget", name = "auto-save", havingValue = "false")
@Slf4j
public class XlsxWorkbookNoOpWriter implements XlsxWorkbookWriter {
	public boolean saveWorkbookAs(final SXSSFWorkbook workbook, final File saveAsFilePathWithName) {
		log.warn("Workbook not saved as {}. Using XlsxWorkbookNoOpWriter", saveAsFilePathWithName.getAbsolutePath());

		return true;
	}
}
