package org.heiner.budget.xlsx.util;

import java.io.File;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public interface XlsxWorkbookWriter {
	boolean saveWorkbookAs(final SXSSFWorkbook workbook, final File saveAsFilePathWithName);
}
