package org.heiner.budget.xlsx.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component(value = "xlsxWorkbookWriter")
@Profile(value = { "default" })
@ConditionalOnProperty(prefix = "annual-budget", name = "auto-save", havingValue = "true")
@Slf4j
public class XlsxWorkbookAutoSaveWriter implements XlsxWorkbookWriter {
	@Override
	public boolean saveWorkbookAs(final SXSSFWorkbook workbook, final File saveAsFilePathWithName) {
		log.info("Saving workbook as {}", saveAsFilePathWithName.getAbsolutePath());

		try {
			final PrintStream xlsxStream = new PrintStream(
					new BufferedOutputStream(new FileOutputStream(saveAsFilePathWithName)));
			workbook.write(xlsxStream);
			workbook.close();
			workbook.dispose();

			if (xlsxStream != null) {
				xlsxStream.close();
			}
		} catch (final Exception e) {
			log.error("Failed to save workbook", e);
			return false;
		}

		log.info("File successfully saved");

		return true;
	}
}
