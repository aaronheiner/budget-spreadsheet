package org.heiner.budget.xlsx;

import org.apache.poi.ss.util.CellReference;
import org.springframework.stereotype.Component;

@Component
public class XlsxCellFormula {
	public String sumForRange(final CellReference startCell, final CellReference endCell) {
		CellReference.convertNumToColString(startCell.getCol());
		return String.format("=SUM(%s%s:%s%s)", CellReference.convertNumToColString(startCell.getCol()),
				startCell.getRow() + 1, CellReference.convertNumToColString(endCell.getCol()),
				endCell.getRow() + 1);
	}
}
