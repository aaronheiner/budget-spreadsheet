package org.heiner.budget.xlsx;

import static org.heiner.budget.util.StreamUtility.withSelfIncrementingCounter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.heiner.budget.domain.BudgetRecord;
import org.heiner.budget.domain.BudgetSection;
import org.heiner.budget.domain.DailyLedger;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class MonthlyBudgetXlsxBudgetSectionAppender {
	private static final int CATEGORY_COLUMN_INDEX = 1;
	private static final int CATEGORY_TOTAL_COLUMN_INDEX = 2;
	private final XlsxCellFormula cellFormula;

	public int addBudgetSectionToSheetAndReturnFinishRowIndex(final BudgetSection budgetSection, final SXSSFSheet sheet,
			final int sectionRowOffset, final AnnualBudgetStyles workbookStyles) {

		writeSectionHeader(budgetSection, sheet.createRow(sectionRowOffset), workbookStyles.getSectionHeaderStyle());

		budgetSection.getBudgetRecords().forEach(withSelfIncrementingCounter((categoryIndex, categoryRecord) -> {
			final Row categoryRow = sheet.createRow(categoryIndex + sectionRowOffset);
			writeBudgetRecord(budgetSection, categoryRecord, categoryRow, workbookStyles);
		}));

		final int sectionSummaryRow = sectionRowOffset + budgetSection.getBudgetRecords().size() + 1;

		writeSectionSummary(budgetSection, sectionRowOffset + 1, sectionSummaryRow - 1,
				sheet.createRow(sectionSummaryRow), workbookStyles);

		return sectionSummaryRow;
	}

	private void writeSectionHeader(final BudgetSection budgetSection, final Row sectionHeaderRow,
			final CellStyle sectionHeaderStyle) {
		sectionHeaderRow.getSheet()
				.addMergedRegion(new CellRangeAddress(sectionHeaderRow.getRowNum(), sectionHeaderRow.getRowNum(),
						CATEGORY_COLUMN_INDEX, CATEGORY_TOTAL_COLUMN_INDEX + budgetSection.getDaysInMonth()));
		final Cell sectionTitleCell = sectionHeaderRow.createCell(CATEGORY_COLUMN_INDEX);
		sectionTitleCell.setCellValue(budgetSection.getTitle());
		sectionTitleCell.setCellStyle(sectionHeaderStyle);
	}

	private void writeBudgetRecord(final BudgetSection budgetSection, final BudgetRecord budgetRecord,
			final Row categoryRow, final AnnualBudgetStyles workbookStyles) {

		final Cell categoryCell = categoryRow.createCell(CATEGORY_COLUMN_INDEX);
		categoryCell.setCellValue(budgetRecord.getBudgetCategory().getDescription());
		categoryCell.setCellStyle(workbookStyles.getCategoryStyle());

		final Cell categoryTotalCell = categoryRow.createCell(CATEGORY_TOTAL_COLUMN_INDEX);

		final CellReference startCell = new CellReference(categoryRow.getRowNum(), CATEGORY_TOTAL_COLUMN_INDEX + 1);
		final CellReference endCell = new CellReference(categoryRow.getRowNum(),
				CATEGORY_TOTAL_COLUMN_INDEX + budgetSection.getDaysInMonth());

		categoryTotalCell.setCellFormula(cellFormula.sumForRange(startCell, endCell));
		categoryTotalCell.setCellStyle(workbookStyles.getCategoryTotalStyle());

		for (DailyLedger dailyLedger : budgetRecord.getDailyLedgers()) {
			final Cell dayOfMonthCell = categoryRow
					.createCell(CATEGORY_TOTAL_COLUMN_INDEX + dailyLedger.getDayOfMonth());
			dayOfMonthCell.setBlank();
			if (dailyLedger.isWeekend()) {
				dayOfMonthCell.setCellStyle(workbookStyles.getWeekendStyle());
			} else {
				dayOfMonthCell.setCellStyle(workbookStyles.getWeekdayStyle());
			}
		}
	}

	private void writeSectionSummary(final BudgetSection budgetSection, final int sectionCategoryFirstRowIndex,
			final int sectionCategoryLastRowIndex, final Row sectionSummaryRow,
			final AnnualBudgetStyles workbookStyles) {

		final Cell summaryTitleCell = sectionSummaryRow.createCell(CATEGORY_COLUMN_INDEX);
		summaryTitleCell.setCellValue(String.format("TOTAL %s", budgetSection.getTitle().toUpperCase()));
		summaryTitleCell.setCellStyle(workbookStyles.getSectionSummaryStyle());

		final Cell sectionSummaryTotalCell = sectionSummaryRow.createCell(CATEGORY_TOTAL_COLUMN_INDEX);

		CellReference startCell = new CellReference(sectionCategoryFirstRowIndex, CATEGORY_TOTAL_COLUMN_INDEX);
		CellReference endCell = new CellReference(sectionCategoryLastRowIndex, CATEGORY_TOTAL_COLUMN_INDEX);

		sectionSummaryTotalCell.setCellFormula(cellFormula.sumForRange(startCell, endCell));
		sectionSummaryTotalCell.setCellStyle(workbookStyles.getSectionSummaryStyle());

		for (int dayOfMonth = 1; dayOfMonth <= budgetSection.getDaysInMonth(); dayOfMonth++) {
			int currentColumnIndex = CATEGORY_TOTAL_COLUMN_INDEX + dayOfMonth;

			final Cell dayOfMonthSummaryCell = sectionSummaryRow.createCell(currentColumnIndex);

			startCell = new CellReference(sectionCategoryFirstRowIndex, currentColumnIndex);
			endCell = new CellReference(sectionCategoryLastRowIndex, currentColumnIndex);

			dayOfMonthSummaryCell.setCellFormula(cellFormula.sumForRange(startCell, endCell));
			dayOfMonthSummaryCell.setCellStyle(workbookStyles.getSectionSummaryStyle());
		}
	}
}
