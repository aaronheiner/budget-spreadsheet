package org.heiner.budget.xlsx;

import java.io.File;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.config.BudgetProperties;
import org.heiner.budget.domain.AnnualBudget;
import org.heiner.budget.xlsx.util.XlsxWorkbookWriter;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class AnnualBudgetXlsxWorkbookGenerator {
	private final BudgetProperties budgetProperties;
	private final AnnualBudget annualBudget;
	private final MonthlyBudgetXlsxSheetAppender monthlyBudgetXlsxSheetWriter;
	private final XlsxWorkbookWriter xlsxWorkbookWriter;

	@EventListener(ApplicationStartedEvent.class)
	public void generatedAndSaveWorkbook() {
		final SXSSFWorkbook workbook = generateWorkbookForBudget();
		final File saveAsFilePathWithName = getSaveAsFilePathWithName();
		xlsxWorkbookWriter.saveWorkbookAs(workbook, saveAsFilePathWithName);
	}

	private SXSSFWorkbook generateWorkbookForBudget() {
		log.info("Generating workbook for annual budget {}", annualBudget.getBudgetYear());

		final int UNLIMITED_ACCESS = -1;
		final SXSSFWorkbook workbook = new SXSSFWorkbook(UNLIMITED_ACCESS);

		final AnnualBudgetStyles workbookStyles = AnnualBudgetStyles.forWorkbook(workbook);

		annualBudget.getMonthlyBudgets().forEach(monthlyBudget -> {
			monthlyBudgetXlsxSheetWriter.appendSheetForMonthlyBudgetToWorkbookUsingStyles(monthlyBudget, workbook,
					workbookStyles);
		});

		log.info("Workbook successfully generated");

		return workbook;
	}

	private File getSaveAsFilePathWithName() {
		return new File(budgetProperties.getWorkingDirectory(),
				String.format("Budget %s", annualBudget.getBudgetYear()).concat(".xlsx"));
	}
}
