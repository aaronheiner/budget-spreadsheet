package org.heiner.budget.xlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.heiner.budget.domain.MonthlyBudget;
import org.springframework.stereotype.Component;

@Component
class MonthlyBudgetXlsxTitleAppender {
	private static final int TITLE_START_ROW_INDEX = 0;
	private static final int TITLE_END_ROW_INDEX = 1;
	private static final int TITLE_COLUMN_START_INDEX = 1;
	private static final int TITLE_COLUMN_END_INDEX = 2;

	public void appendMonthlyBudgetTitleToSheetUsingStyles(final MonthlyBudget monthlyBudget, SXSSFSheet sheet,
			final AnnualBudgetStyles workbookStyles) {
		final Row titleRow = sheet.createRow(TITLE_START_ROW_INDEX);
		sheet.addMergedRegion(new CellRangeAddress(TITLE_START_ROW_INDEX, TITLE_END_ROW_INDEX, TITLE_COLUMN_START_INDEX,
				TITLE_COLUMN_END_INDEX));

		final Cell sheetTitleCell = titleRow.createCell(TITLE_COLUMN_START_INDEX);
		sheetTitleCell.setCellValue(monthlyBudget.getTitle());
		sheetTitleCell.setCellStyle(workbookStyles.getSheetTitleStyle());
	}
}
