package org.heiner.budget.domain;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.heiner.budget.category.BudgetCategory;
import org.heiner.budget.category.ExpenseCategory;
import org.junit.jupiter.api.Test;

class ExpenseSectionTests {
	@Test
	void shouldReturnBudgetRecordForEachExpenseCategory() {
		// Given
		final ExpenseSection expenseSection = ExpenseSection.forMonthAndBudgetYear(Month.JANUARY, 2022);

		// When
		List<BudgetRecord> budgetRecords = expenseSection.getBudgetRecords();

		// Then
		List<BudgetCategory> budgetCategories = budgetRecords.stream()
				.flatMap(budgetRecord -> Stream.of(budgetRecord.getBudgetCategory())).collect(toList());
		assertEquals(ExpenseCategory.values().length, budgetCategories.size());
		assertTrue(budgetCategories.containsAll(List.of(ExpenseCategory.values())));
	}
}
