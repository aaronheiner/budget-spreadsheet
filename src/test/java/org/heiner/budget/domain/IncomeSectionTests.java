package org.heiner.budget.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;

import org.heiner.budget.category.BudgetCategory;
import org.heiner.budget.category.IncomeCategory;
import org.junit.jupiter.api.Test;

class IncomeSectionTests {
	@Test
	void shouldReturnBudgetRecordForEachIncomeCategory() {
		// Given
		final IncomeSection incomeSection = IncomeSection.forMonthAndBudgetYear(Month.JANUARY, 2022);

		// When
		List<BudgetRecord> budgetRecords = incomeSection.getBudgetRecords();

		// Then
		List<BudgetCategory> budgetCategories = budgetRecords.stream()
				.flatMap(budgetRecord -> Stream.of(budgetRecord.getBudgetCategory())).collect(toList());
		assertEquals(IncomeCategory.values().length, budgetCategories.size());
		assertTrue(budgetCategories.containsAll(List.of(IncomeCategory.values())));
	}
}
