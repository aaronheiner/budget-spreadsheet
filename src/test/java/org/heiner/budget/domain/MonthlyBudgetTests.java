package org.heiner.budget.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class MonthlyBudgetTests {
	private static final int BUDGET_YEAR_FOR_TESTS = 2022;

	@Test
	void shouldReturnConstructedMonthlyBudgetForCorrectMonth() {
		// Given
		final Month monthForTest = Month.JANUARY;

		// When
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(monthForTest, BUDGET_YEAR_FOR_TESTS);

		// Then
		assertNotNull(monthlyBudget);
		assertEquals(monthForTest.getDescription(), monthlyBudget.getMonthName());
	}

	@Test
	void shouldReturnTitleContainingMonthNameAndBudgetYear() {
		// Given
		final String expectedTitle = "February 2022";

		// When
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.FEBRUARY, BUDGET_YEAR_FOR_TESTS);

		// Then
		assertEquals(expectedTitle, monthlyBudget.getTitle());
	}

	@Test
	void shouldReturnTwentyNineDaysForFebruaryTwentyTwenty() {
		// Given
		final int expectTwentyNineDaysInMonth = 29;

		// When
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.FEBRUARY, 2020);

		// Then
		assertEquals(expectTwentyNineDaysInMonth, monthlyBudget.getDaysInMonth());
	}

	@Test
	void shouldReturnTwentyEightDaysForFebruaryTwentyTwentyTwo() {
		// Given
		final int expectTwentyEightDaysInMonth = 28;

		// When
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.FEBRUARY, 2022);

		// Then
		assertEquals(expectTwentyEightDaysInMonth, monthlyBudget.getDaysInMonth());
	}
}
