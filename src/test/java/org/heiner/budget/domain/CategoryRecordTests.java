package org.heiner.budget.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.heiner.budget.category.ExpenseCategory;
import org.junit.jupiter.api.Test;

class CategoryRecordTests {
	@Test
	void shouldReturnTwentyNineDailyLedgerForFebruaryOfALeapYear() {
		// Given
		final int leapYear = 2020;
		final int expectTwentyNineDailyLedgers = 29;

		// When
		BudgetRecord budgetRecord = CategoryRecord.forBudgetCategory(ExpenseCategory.HOBBY, Month.FEBRUARY, leapYear);

		// Then
		assertNotNull(budgetRecord);
		assertEquals(expectTwentyNineDailyLedgers, budgetRecord.getDailyLedgers().size());
	}

	@Test
	void shouldReturnTwentyEightDailyLedgerForFebruaryOfANonLeapYear() {
		// Given
		final int nonLeapYear = 2022;
		final int expectTwentyEightDailyLedgers = 28;

		// When
		BudgetRecord budgetRecord = CategoryRecord.forBudgetCategory(ExpenseCategory.HOBBY, Month.FEBRUARY, nonLeapYear);

		// Then
		assertNotNull(budgetRecord);
		assertEquals(expectTwentyEightDailyLedgers, budgetRecord.getDailyLedgers().size());
	}
}
