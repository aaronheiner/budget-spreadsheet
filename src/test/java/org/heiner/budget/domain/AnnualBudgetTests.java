package org.heiner.budget.domain;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.heiner.budget.config.BudgetProperties;
import org.heiner.budget.config.account.AccountConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AccountConfiguration.class, BudgetProperties.class, AnnualBudget.class })
class AnnualBudgetTests {
	@Autowired
	private BudgetProperties budgetProperties;

	@Autowired
	private AnnualBudget annualBudget;

	@Test
	void shouldReturnCorrectBudgetYear() {
		// When
		final int budgetYear = annualBudget.getBudgetYear();

		// Then
		assertEquals(budgetProperties.getYear(), budgetYear);
	}

	@Test
	void shouldReturnMonthlyBudgetForEachMonth() {
		// When
		final List<MonthlyBudget> monthlyBudgets = annualBudget.getMonthlyBudgets();

		// Then
		List<Month> budgetMonths = monthlyBudgets.stream().flatMap(monthlyBudget -> Stream.of(monthlyBudget.getMonth()))
				.collect(toList());
		assertEquals(Month.values().length, monthlyBudgets.size());
		assertTrue(budgetMonths.containsAll(List.of(Month.values())));
	}
}
