package org.heiner.budget.xlsx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.category.ExpenseCategory;
import org.heiner.budget.category.IncomeCategory;
import org.heiner.budget.domain.BudgetSection;
import org.heiner.budget.domain.ExpenseSection;
import org.heiner.budget.domain.IncomeSection;
import org.heiner.budget.domain.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

class MonthlyBudgetXlsxBudgetSectionAppenderTests {
	private MonthlyBudgetXlsxBudgetSectionAppender monthlyBudgetXlsxBudgetSectionAppender;
	private SXSSFWorkbook workbook;

	@BeforeEach
	void setup() {
		monthlyBudgetXlsxBudgetSectionAppender = new MonthlyBudgetXlsxBudgetSectionAppender(new XlsxCellFormula());
		workbook = new SXSSFWorkbook();
	}

	@AfterEach
	@SneakyThrows
	void teardown() {
		workbook.dispose();
		workbook.close();
	}

	@Test
	void shouldAppendIncomeSectionWithHeaderCategoriesAndSummary() {
		// Given
		final BudgetSection incomeSection = IncomeSection.forMonthAndBudgetYear(Month.FEBRUARY, 2020);
		final AnnualBudgetStyles workbookStyles = AnnualBudgetStyles.forWorkbook(workbook);
		final SXSSFSheet sheet = workbook.createSheet();
		final int sectionStartRowIndex = 1;
		final int headerAndFooterRows = 2;
		final int expectedSectionRowsAdded = IncomeCategory.values().length + headerAndFooterRows;

		// When
		final int sectionEndRowIndex = monthlyBudgetXlsxBudgetSectionAppender
				.addBudgetSectionToSheetAndReturnFinishRowIndex(incomeSection, sheet, sectionStartRowIndex,
						workbookStyles);

		// Then
		assertEquals(expectedSectionRowsAdded, sectionEndRowIndex);
	}

	@Test
	void shouldAppendExpenseSectionWithHeaderCategoriesAndSummary() {
		// Given
		final BudgetSection expenseSection = ExpenseSection.forMonthAndBudgetYear(Month.FEBRUARY, 2020);
		final AnnualBudgetStyles workbookStyles = AnnualBudgetStyles.forWorkbook(workbook);
		final SXSSFSheet sheet = workbook.createSheet();
		final int sectionStartRowIndex = 1;
		final int headerAndFooterRows = 2;
		final int expectedSectionRowsAdded = ExpenseCategory.values().length + headerAndFooterRows;

		// When
		final int sectionEndRowIndex = monthlyBudgetXlsxBudgetSectionAppender
				.addBudgetSectionToSheetAndReturnFinishRowIndex(expenseSection, sheet, sectionStartRowIndex,
						workbookStyles);

		// Then
		assertEquals(expectedSectionRowsAdded, sectionEndRowIndex);
	}
}
