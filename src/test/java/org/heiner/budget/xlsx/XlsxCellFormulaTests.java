package org.heiner.budget.xlsx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.poi.ss.util.CellReference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class XlsxCellFormulaTests {
	private XlsxCellFormula xlsxCellFormula;

	@BeforeEach
	void setup() {
		xlsxCellFormula = new XlsxCellFormula();
	}

	@Test
	void shouldReturnCorrectSumFormulaForRange() {
		// Given
		final CellReference startingCell = new CellReference(1, 1);
		final CellReference endingCell = new CellReference(1, 26);
		final String expectedFormula = "=SUM(B2:AA2)";

		// When
		final String generatedFormula = xlsxCellFormula.sumForRange(startingCell, endingCell);

		// Then
		assertEquals(expectedFormula, generatedFormula);
	}
}
