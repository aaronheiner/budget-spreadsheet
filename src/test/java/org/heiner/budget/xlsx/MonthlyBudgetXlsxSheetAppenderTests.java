package org.heiner.budget.xlsx;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.domain.BudgetSection;
import org.heiner.budget.domain.Month;
import org.heiner.budget.domain.MonthlyBudget;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MonthlyBudgetXlsxSheetAppenderTests {
	private MonthlyBudgetXlsxTitleAppender mockBudgetTitleAppender;
	private MonthlyBudgetXlsxCategoryHeaderAppender mockBudgetCategorHeaderAppender;
	private MonthlyBudgetXlsxBudgetSectionAppender mockBudgetSectionAppender;

	private MonthlyBudgetXlsxSheetAppender monthlySheetAppender;

	private static MonthlyBudget testMonthlyBudget;
	private static SXSSFWorkbook mockWorkbook;
	private static SXSSFSheet mockSheet;
	private static AnnualBudgetStyles mockBudgetStyles;

	@BeforeAll
	static void oneTimeSetup() {
		testMonthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.DECEMBER, 2022);

		mockWorkbook = mock(SXSSFWorkbook.class);
		mockSheet = mock(SXSSFSheet.class);
		when(mockWorkbook.createSheet(any(String.class))).thenReturn(mockSheet);

		mockBudgetStyles = mock(AnnualBudgetStyles.class);
	}

	@BeforeEach
	void setup() {
		mockBudgetTitleAppender = mock(MonthlyBudgetXlsxTitleAppender.class);
		mockBudgetCategorHeaderAppender = mock(MonthlyBudgetXlsxCategoryHeaderAppender.class);
		mockBudgetSectionAppender = mock(MonthlyBudgetXlsxBudgetSectionAppender.class);

		monthlySheetAppender = new MonthlyBudgetXlsxSheetAppender(mockBudgetTitleAppender,
				mockBudgetCategorHeaderAppender, mockBudgetSectionAppender);
	}

	@Test
	void shouldAppendTitleOnceUsingMonthlyBudgetXlsxTitleAppenderWhenAppendSheetForBudget() {
		// Given
		final int EXPECT_CALLED_ONCE = 1;

		// When
		monthlySheetAppender.appendSheetForMonthlyBudgetToWorkbookUsingStyles(testMonthlyBudget, mockWorkbook,
				mockBudgetStyles);

		// Then
		verify(mockBudgetTitleAppender, times(EXPECT_CALLED_ONCE))
				.appendMonthlyBudgetTitleToSheetUsingStyles(testMonthlyBudget, mockSheet, mockBudgetStyles);
	}

	@Test
	void shouldAppendCategoryHeaderOnceForMonthlyBudgetWhenAppendSheetForBudget() {
		// Given
		final int EXPECT_CALLED_ONCE = 1;

		// When
		monthlySheetAppender.appendSheetForMonthlyBudgetToWorkbookUsingStyles(testMonthlyBudget, mockWorkbook,
				mockBudgetStyles);

		// Then
		verify(mockBudgetCategorHeaderAppender, times(EXPECT_CALLED_ONCE))
				.appendCategoryHeaderToSheetUsingStylesAndReturnRowIndex(testMonthlyBudget, mockSheet,
						mockBudgetStyles);
	}

	@Test
	void shouldAppendBothBudgetSectionsUsingMonthlyBudgetXlsxBudgetSectionAppenderWhenAppendSheetForBudget() {
		// Given
		final int EXPECT_CALLED_TWICE = 2;

		// When
		monthlySheetAppender.appendSheetForMonthlyBudgetToWorkbookUsingStyles(testMonthlyBudget, mockWorkbook,
				mockBudgetStyles);

		// Then
		verify(mockBudgetSectionAppender, times(EXPECT_CALLED_TWICE)).addBudgetSectionToSheetAndReturnFinishRowIndex(
				any(BudgetSection.class), eq(mockSheet), any(Integer.class), eq(mockBudgetStyles));
	}
}
