package org.heiner.budget.xlsx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.domain.Month;
import org.heiner.budget.domain.MonthlyBudget;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

class MonthlyBudgetXlsxCategoryHeaderAppenderTests {
	private MonthlyBudgetXlsxCategoryHeaderAppender monthlyBudgetXlsxCategoryHeaderAppender;
	private SXSSFWorkbook workbook;

	@BeforeEach
	void setup() {
		monthlyBudgetXlsxCategoryHeaderAppender = new MonthlyBudgetXlsxCategoryHeaderAppender();
		workbook = new SXSSFWorkbook();
	}

	@AfterEach
	@SneakyThrows
	void teardown() {
		workbook.dispose();
		workbook.close();
	}

	@Test
	void shouldAppendMonthlyBudgetCategoryHeaderToSheet() {
		// Given
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.MARCH, 2022);
		final AnnualBudgetStyles workbookStyles = AnnualBudgetStyles.forWorkbook(workbook);
		final SXSSFSheet sheet = workbook.createSheet();
		final int categoryHeaderStartIndex = 1;
		final int categoryHeaderDayOneColumnIndex = 3;
		final int categoryHeaderDayThirtyOneColumnIndex = 33;

		// When
		final int categoryHeaderRowIndex = monthlyBudgetXlsxCategoryHeaderAppender
				.appendCategoryHeaderToSheetUsingStylesAndReturnRowIndex(monthlyBudget, sheet, workbookStyles);

		// Then
		final Row categoryRow = sheet.getRow(categoryHeaderRowIndex);
		assertEquals("Category", categoryRow.getCell(categoryHeaderStartIndex).getStringCellValue());
		assertEquals(1, categoryRow.getCell(categoryHeaderDayOneColumnIndex).getNumericCellValue());
		assertEquals(31, categoryRow.getCell(categoryHeaderDayThirtyOneColumnIndex).getNumericCellValue());
	}

}
