package org.heiner.budget.xlsx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.domain.Month;
import org.heiner.budget.domain.MonthlyBudget;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

class MonthlyBudgetXlsxTitleAppenderTests {
	private MonthlyBudgetXlsxTitleAppender monthlyBudgetXlsxTitleAppender;
	private SXSSFWorkbook workbook;

	@BeforeEach
	void setup() {
		monthlyBudgetXlsxTitleAppender = new MonthlyBudgetXlsxTitleAppender();
		workbook = new SXSSFWorkbook();
	}

	@AfterEach
	@SneakyThrows
	void teardown() {
		workbook.dispose();
		workbook.close();
	}

	@Test
	void shouldAppendMonthlyBudgetTitleToSheet() {
		// Given
		final MonthlyBudget monthlyBudget = MonthlyBudget.forMonthAndBudgetYear(Month.FEBRUARY, 2022);
		final AnnualBudgetStyles workbookStyles = AnnualBudgetStyles.forWorkbook(workbook);
		final SXSSFSheet sheet = workbook.createSheet();
		final int expectedTitleRowIndex = 0;
		final int expectedTitleColumnIndex = 1;

		// When
		monthlyBudgetXlsxTitleAppender.appendMonthlyBudgetTitleToSheetUsingStyles(monthlyBudget, sheet, workbookStyles);

		// Then
		final String titleFromSheet = sheet.getRow(expectedTitleRowIndex).getCell(expectedTitleColumnIndex)
				.getStringCellValue();
		assertEquals(monthlyBudget.getTitle(), titleFromSheet);
	}
}
