package org.heiner.budget.xlsx;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.heiner.budget.config.BudgetProperties;
import org.heiner.budget.config.account.AccountResolver;
import org.heiner.budget.domain.AnnualBudget;
import org.heiner.budget.domain.MonthlyBudget;
import org.heiner.budget.xlsx.util.XlsxWorkbookWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

class AnnualBudgetXlsxWorkbookGeneratorTests {
	private AnnualBudget annualBudget;
	private AnnualBudgetXlsxWorkbookGenerator annualBudgetWriter;
	private MonthlyBudgetXlsxSheetAppender mockMonthlySheetAppender;
	private XlsxWorkbookWriter mockXlsxWorkbookWriter;

	@BeforeEach
	void setup() {
		final BudgetProperties budgetProperties = new BudgetProperties();
		budgetProperties.setYear(2022);

		final AccountResolver accountResolver = mock(AccountResolver.class);

		annualBudget = new AnnualBudget(budgetProperties, accountResolver);

		mockMonthlySheetAppender = mock(MonthlyBudgetXlsxSheetAppender.class);
		mockXlsxWorkbookWriter = mock(XlsxWorkbookWriter.class);

		annualBudgetWriter = new AnnualBudgetXlsxWorkbookGenerator(budgetProperties, annualBudget,
				mockMonthlySheetAppender, mockXlsxWorkbookWriter);
	}

	@Test
	@SneakyThrows
	void shouldAppendSheetsForEachMonthContainedInTheAnnualBudget() {
		// When
		annualBudgetWriter.generatedAndSaveWorkbook();

		// Then
		verify(mockMonthlySheetAppender, times(annualBudget.getMonthlyBudgets().size()))
				.appendSheetForMonthlyBudgetToWorkbookUsingStyles(any(MonthlyBudget.class), any(SXSSFWorkbook.class),
						any(AnnualBudgetStyles.class));
	}

	@Test
	@SneakyThrows
	void shouldSaveTheGeneratedWorkbook() {
		// Given
		final int EXPECT_SAVED_ONCE = 1;

		// When
		annualBudgetWriter.generatedAndSaveWorkbook();

		// Then
		verify(mockXlsxWorkbookWriter, times(EXPECT_SAVED_ONCE)).saveWorkbookAs(any(SXSSFWorkbook.class),
				any(File.class));
	}
}
