package org.heiner.budget.config.account;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AccountConfiguration.class })
class AccountConfigurationTests {
	@Autowired
	private AccountResolver accountResolver;

	@Test
	void shouldResolveAccountForAllNamedAccount() {
		// When
		NamedAccount.stream().forEach(namedAccount -> {
			// Then
			assertNotNull(accountResolver.getNamedAccount(namedAccount));
		});
	}
}
