package org.heiner.budget.config.account;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Optional;

import org.heiner.budget.config.ConfigurationException;
import org.heiner.budget.domain.account.Account;
import org.heiner.budget.domain.account.CheckingAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountResolverTests {
	private AccountResolver accountResolver;

	@BeforeEach
	void setup() {
		accountResolver = new AccountResolver();
	}

	@Test
	void shouldResolveAccountWhenAccountPreviouslyAddedToResolver() {
		// Given
		final Account mappedAccount = new CheckingAccount("Test Account", BigDecimal.ONE, Optional.empty());
		accountResolver.addNamedAccount(NamedAccount.LMCU_MAX_CHECKING, mappedAccount);

		// When
		final Account resolvedAccount = accountResolver.getNamedAccount(NamedAccount.LMCU_MAX_CHECKING);

		// Then
		assertEquals(mappedAccount, resolvedAccount);
	}

	@Test
	void shouldThrowConfigurationExceptionWhenAccountNotPreviouslyAddedToResolver() {
		// Then
		assertThrows(ConfigurationException.class, () -> {
			// When
			accountResolver.getNamedAccount(NamedAccount.ROCKET_MORTGAGE);
		});
	}
}
