package org.heiner.budget.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

class StreamUtilityTests {
	@Nested
	class WithSelfIncrementingCounterTests {
		@Test
		void shouldAutomaticallyIncrementCounterDuringStreamProcessing() {
			// Given
			final SelfIncrementingCounterTestSupport mock = Mockito.mock(SelfIncrementingCounterTestSupport.class);
			final List<String> objectsToIterate = List.of("One", "Two", "Three");

			// When
			objectsToIterate.forEach(StreamUtility.withSelfIncrementingCounter((selfIncrementedCounter, listItem) -> {
				mock.iteratedMethodToCall(selfIncrementedCounter);
			}));

			// Then
			final ArgumentCaptor<Integer> selfIncrementedCounterCaptor = ArgumentCaptor.forClass(Integer.class);

			verify(mock, times(objectsToIterate.size())).iteratedMethodToCall(selfIncrementedCounterCaptor.capture());

			final Integer capturedCounterValue = selfIncrementedCounterCaptor.getValue();

			assertEquals(objectsToIterate.size(), capturedCounterValue);
		}
	}

	private interface SelfIncrementingCounterTestSupport {
		void iteratedMethodToCall(final Integer selfIncrementedCounter);
	}
}
